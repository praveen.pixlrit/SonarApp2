package com.myapp.abc.music_app;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

/**
 * Created by abc on 22/04/2016.
 */
public class Playlist_fragment extends Fragment {
    RelativeLayout r1;

    public Playlist_fragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_library, container, false);
        r1 = (RelativeLayout) view.findViewById(R.id.r1);
        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Title_screen.class);
                startActivity(i);
            }
        });
        return view;
    }
}
