package com.myapp.abc.music_app;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by abc on 22/04/2016.
 */
public class Downloded_books extends Activity {
    ImageView search_button;
    TextView txt_head;
    RelativeLayout rlv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.downloded_books);
        search_button = (ImageView) findViewById(R.id.search);
        txt_head = (TextView) findViewById(R.id.txt_head);
        rlv = (RelativeLayout) findViewById(R.id.rlv);
        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_head.setVisibility(View.INVISIBLE);
                rlv.setVisibility(View.VISIBLE);
            }
        });
    }
}
