package com.myapp.abc.music_app;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by abc on 21/04/2016.
 */
public class My_Library extends Fragment {
    RelativeLayout r1;
    ImageView i1;
    Button browse;
    LinearLayout l1, l2;
    Button btn_overflow1, btn_overflow2;
    Point p;
    PopupWindow popUp;
    LinearLayout layout;
    TextView tv;
    ViewGroup.LayoutParams params;
    LinearLayout mainLayout;
    Button but;
    boolean click = true;
    private static final int PREFERENCE_PRIVATE_MODE = 0;
    SharedPreferences sh_pref;
    SharedPreferences.Editor toEdit;
    String fileurl;
    RadioButton like;
    String dtStart;
    Date date;
    ProgressBar progressBar;
    TextView exptxt;
    String song_date;
    String current_date;
    long difference_date;
    String dayDifference;
    int myno = 0;

    public My_Library() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.your_shelf, container, false);
        sh_pref = getActivity().getSharedPreferences("shref", PREFERENCE_PRIVATE_MODE);
        toEdit = sh_pref.edit();
        r1 = (RelativeLayout) view.findViewById(R.id.r1);
        i1 = (ImageView) view.findViewById(R.id.i1);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar1);
        exptxt = (TextView) view.findViewById(R.id.exp);
       /* if (sh_pref.contains("formattedDate")) {
            i1.setVisibility(View.VISIBLE);
            r1.setVisibility(View.VISIBLE);
            exptxt.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            fileurl = sh_pref.getString("file_url", "");
            dtStart = sh_pref.getString("formattedDate", "");

            Log.e("fileurl", fileurl);
            try {
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                date = format.parse(dtStart);
                System.out.println(date);
                Log.e("dateeeeeee", date.toString());
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            getFutureDate(date, 14);
        }*/
       /* if (dtStart.equalsIgnoreCase("")) {

        } else {
            i1.setVisibility(View.VISIBLE);
            r1.setVisibility(View.VISIBLE);
            exptxt.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
        }*/

        if (sh_pref.contains("formattedDate")) {
            dtStart = sh_pref.getString("formattedDate", "");
            song_date = sh_pref.getString("date", "");
            Log.e("song_date", song_date);
            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            String formattedDate = df.format(c.getTime());
            Log.e("newtime", formattedDate);
            String[] separated = formattedDate.split(" ");
            current_date = separated[0];
            String time = separated[1];
            Log.e("current_date", current_date);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date startDate = null;
            Date endDate = null;
            try {
                startDate = simpleDateFormat.parse("08:00 AM");
                endDate = simpleDateFormat.parse("04:00 PM");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.e("stendate", startDate + "  " + endDate);

            long difference = endDate.getTime() - startDate.getTime();
       /* if (difference <= 5) {
            playSong();
        } else {
            Toast.makeText(getApplicationContext(), "Timeout", Toast.LENGTH_LONG).show();
        }*/
            Date date1, date2;
            SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy");
            try {
                date1 = dates.parse(current_date);
                date2 = dates.parse(song_date);
                difference_date = Math.abs(date1.getTime() - date2.getTime());
                long differenceDates = difference / (24 * 60 * 60 * 1000);
                dayDifference = Long.toString(differenceDates);
                Log.e("d1d2", dayDifference);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            try {
                myno = Integer.parseInt(dayDifference);
                Log.e("myno", String.valueOf(myno));
            } catch (NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }
            try {
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                date = format.parse(dtStart);
                System.out.println(date);
                Log.e("dateeeeeee", date.toString());
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            getFutureDate(date, 14);
            if (myno <= 1) {
                i1.setVisibility(View.VISIBLE);
                r1.setVisibility(View.VISIBLE);
                exptxt.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);

            } else {
                i1.setVisibility(View.INVISIBLE);
                r1.setVisibility(View.INVISIBLE);
                exptxt.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.progress);
        progressBar.setMax(15);
        progressBar.setProgress(1);   // Main Progress
        // progressBar.setSecondaryProgress(50); // Secondary Progress
        // Maximum Progress
        //  progressBar.setProgressDrawable(drawable);

        like = (RadioButton) view.findViewById(R.id.like);
        btn_overflow1 = (Button) view.findViewById(R.id.btn_overflow);
        btn_overflow2 = (Button) view.findViewById(R.id.btn_overflow1);
        l1 = (LinearLayout) view.findViewById(R.id.l1);
        l2 = (LinearLayout) view.findViewById(R.id.l2);
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Titleof_track.class);
                startActivity(i);
            }
        });
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Titleof_track.class);
                startActivity(i);
            }
        });
        browse = (Button) view.findViewById(R.id.browse);
        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Browse_song.class);
                startActivity(i);
            }
        });
        btn_overflow1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater ly = (LayoutInflater) getActivity().getBaseContext()
                        .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
                final View popupView = ly.inflate(R.layout.dialogbrand_layout, null);
                final PopupWindow popupWindow = new PopupWindow(popupView,
                        ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                popupWindow.setFocusable(true);
                popupWindow
                        .setOnDismissListener(new PopupWindow.OnDismissListener() {

                            @Override
                            public void onDismiss() {
                                // TODO Auto-generated method stub

                            }
                        });
                TextView rate = (TextView) popupView.findViewById(R.id.rate);
                rate.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        Intent i = new Intent(getActivity(), Rate_review.class);
                        startActivity(i);
                    }
                });
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.showAsDropDown(btn_overflow1, 0, 0);
                popupView.setFocusable(true);

                popupWindow.getContentView().setOnKeyListener(
                        new View.OnKeyListener() {
                            @Override
                            public boolean onKey(View v, int keyCode,
                                                 KeyEvent event) {
                                if (keyCode == KeyEvent.KEYCODE_BACK) {
                                    popupWindow.dismiss();
                                    return true;
                                }
                                return false;
                            }
                        });

            }

        });
        btn_overflow2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                LayoutInflater ly = (LayoutInflater) getActivity().getBaseContext()
                        .getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
                View popupView = ly.inflate(R.layout.dialogbrand_layout, null);
                final PopupWindow popupWindow = new PopupWindow(popupView,
                        ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);

                popupWindow.setFocusable(true);
                popupWindow
                        .setOnDismissListener(new PopupWindow.OnDismissListener() {

                            @Override
                            public void onDismiss() {
                                // TODO Auto-generated method stub

                            }
                        });
                TextView rate = (TextView) popupView.findViewById(R.id.rate);
                rate.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        Intent i = new Intent(getActivity(), Rate_review.class);
                        startActivity(i);

                    }
                });
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.showAsDropDown(btn_overflow2, 0, 0);
                popupView.setFocusable(true);

                popupWindow.getContentView().setOnKeyListener(
                        new View.OnKeyListener() {
                            @Override
                            public boolean onKey(View v, int keyCode,
                                                 KeyEvent event) {
                                if (keyCode == KeyEvent.KEYCODE_BACK) {
                                    popupWindow.dismiss();
                                    return true;
                                }
                                return false;
                            }
                        });

            }

        });


        return view;
    }

    public void getFutureDate(Date currentDate, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DATE, days);
        Date futureDate = cal.getTime();
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        String datetime = dateformat.format(futureDate);
        System.out.println("Current Date Time : " + datetime);
        Log.e("futuredate", futureDate.toString() + datetime);
        exptxt.setText("EXP: " + datetime);
    }
}
