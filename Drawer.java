package com.myapp.abc.music_app;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class Drawer extends ActionBarActivity {
    private ListView mDrawerList;
    CustomDrawerAdapter adapter;
    SharedPreferences sh_pref;
    SharedPreferences.Editor toEdit;
    private static final int PREFERENCE_PRIVATE_MODE = 0;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    List<DrawerItem> dataList;
    ImageButton imgLeftMenu;
    TextView txt_head, profile, edit;
    Bundle extras;
    String s_img;
    ImageView logo;
    LinearLayout web;
    private static final long delay = 2000L;
    private boolean mRecentlyBackPressed = false;
    ImageView listicon;
    private Handler mExitHandler = new Handler();
    private Runnable mExitRunnable = new Runnable() {

        @Override
        public void run() {
            mRecentlyBackPressed = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.drawer);
        mDrawerList = (ListView) findViewById(R.id.drawer_list_left);
        imgLeftMenu = (ImageButton) findViewById(R.id.imgLeftMenu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        // ============== Define a Custom Header for Navigation
        // drawer=================//
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        extras = getIntent().getExtras();
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.head, null);
        imgLeftMenu = (ImageButton) v.findViewById(R.id.imgLeftMenu);
        View header = getLayoutInflater().inflate(R.layout.header, null);
        txt_head = (TextView) v.findViewById(R.id.txt_head);
        logo = (ImageView) header.findViewById(R.id.logo);
        listicon = (ImageView) v.findViewById(R.id.list);
        listicon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Drawer.this, Downloded_books.class);
                startActivity(i);
            }
        });
        mDrawerList.addHeaderView(header);
        imgLeftMenu.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

				/*
                 * if (mDrawerLayout.isDrawerOpen(mDrawerList_Right)) {
				 * mDrawerLayout.closeDrawer(mDrawerList_Right); }
				 */
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }

            }
        });

        // location = l.locationn(this);

        if (savedInstanceState == null) {
            Fragment fragment = new My_Library();
            FragmentManager frgManager = getFragmentManager();
            frgManager.beginTransaction().replace(R.id.Frame_Layout, fragment)
                    .commit();

        }

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayUseLogoEnabled(false);

        getSupportActionBar().setDisplayShowCustomEnabled(true);

        getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#051111")));

        getSupportActionBar().setIcon(
                new ColorDrawable(getResources().getColor(
                        android.R.color.transparent)));

        getSupportActionBar().setCustomView(v);
        dataList = new ArrayList<DrawerItem>();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Add Drawer Item to dataList
        dataList.add(new DrawerItem("My Library", R.drawable.mylibrary));
        dataList.add(new DrawerItem("Play List", R.drawable.playlist));
        dataList.add(new DrawerItem("Setting", R.drawable.setting));
        dataList.add(new DrawerItem("Exit", R.drawable.exit));
        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item,
                dataList);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            Log.e("src", src);
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.e("Bitmap", "returned");
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            SelectItem(position);

        }

        private void SelectItem(int position) {
            // TODO Auto-generated method stub
            Fragment fragment = null;
            Bundle args = new Bundle();
            FragmentManager frgManager = getFragmentManager();
            switch (position) {
                case 1:
                    fragment = new My_Library();
                    txt_head.setVisibility(View.VISIBLE);
                    txt_head.setText("My Library");
                    frgManager.beginTransaction()
                            .replace(R.id.Frame_Layout, fragment).commit();

                    mDrawerList.setItemChecked(position, true);
                    setTitle(dataList.get(position).getItemName());
                    mDrawerLayout.closeDrawer(mDrawerList);

                    break;
                case 2:
                    fragment = new Playlist_fragment();
                    txt_head.setVisibility(View.VISIBLE);
                    txt_head.setText("Play List");
                    frgManager.beginTransaction()
                            .replace(R.id.Frame_Layout, fragment).commit();

                    mDrawerList.setItemChecked(position, true);
                    setTitle(dataList.get(position).getItemName());
                    mDrawerLayout.closeDrawer(mDrawerList);

                    break;
                case 3:

                    fragment = new Setting_gragment();
                    txt_head.setVisibility(View.VISIBLE);
                    txt_head.setText("Setting");
                    frgManager.beginTransaction()
                            .replace(R.id.Frame_Layout, fragment).commit();

                    mDrawerList.setItemChecked(position, true);
                    setTitle(dataList.get(position).getItemName());
                    mDrawerLayout.closeDrawer(mDrawerList);

                    break;
                case 4:
                    txt_head.setVisibility(View.VISIBLE);
                    txt_head.setText("");
                    fragment = new My_Library();
                    frgManager.beginTransaction()
                            .replace(R.id.Frame_Layout, fragment).commit();

                    mDrawerList.setItemChecked(position, true);
                    setTitle(dataList.get(position - 1).getItemName());
                    mDrawerLayout.closeDrawer(mDrawerList);
                    break;


            }

        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if (mRecentlyBackPressed) {
            mExitHandler.removeCallbacks(mExitRunnable);
            mExitHandler = null;
            super.onBackPressed();
        } else {
            mRecentlyBackPressed = true;
            Toast.makeText(this, "Press again to exit Application",
                    Toast.LENGTH_SHORT).show();
            mExitHandler.postDelayed(mExitRunnable, delay);
        }
    }
}
