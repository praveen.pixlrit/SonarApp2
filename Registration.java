package com.myapp.abc.music_app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by abc on 20/04/2016.
 */
public class Registration extends Activity {
    TextView btn_sendpin, btn_register;
    EditText ed_email, ed_pass, ed_repass, ed_mb, ed_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.registeration);
        btn_sendpin = (TextView) findViewById(R.id.btn_sendpnn);
        btn_register = (TextView) findViewById(R.id.btn_register);
        ed_email = (EditText) findViewById(R.id.ed_eml);
        ed_pass = (EditText) findViewById(R.id.ed_pass);
        ed_repass = (EditText) findViewById(R.id.ed_repass);
        ed_mb = (EditText) findViewById(R.id.ed_mb);
        ed_code = (EditText) findViewById(R.id.ed_veryfycode);
        String fontPath = "fonts/Hero_Light.otf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        btn_sendpin.setTypeface(tf);
        btn_register.setTypeface(tf);
        ed_email.setTypeface(tf);
        ed_pass.setTypeface(tf);
        ed_repass.setTypeface(tf);
        ed_mb.setTypeface(tf);
        ed_code.setTypeface(tf);


        TypefaceSpan typefaceSpan = new Custom_typeface(tf);
        SpannableString spannableString1 = new SpannableString("Email Address");
        spannableString1.setSpan(typefaceSpan, 0, spannableString1.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ed_email.setHint(spannableString1);

        SpannableString spannableString2 = new SpannableString("Password");
        spannableString2.setSpan(typefaceSpan, 0, spannableString2.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ed_pass.setHint(spannableString2);

        SpannableString spannableString3 = new SpannableString("Re-Enter Password");
        spannableString3.setSpan(typefaceSpan, 0, spannableString3.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ed_repass.setHint(spannableString3);

        SpannableString spannableString4 = new SpannableString("Contact No.");
        spannableString4.setSpan(typefaceSpan, 0, spannableString4.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ed_mb.setHint(spannableString4);

        SpannableString spannableString5 = new SpannableString("Verify Code");
        spannableString5.setSpan(typefaceSpan, 0, spannableString5.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ed_code.setHint(spannableString5);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Registration.this, Login.class);
                startActivity(i);
            }
        });

    }
}
