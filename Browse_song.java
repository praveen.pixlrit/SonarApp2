package com.myapp.abc.music_app;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abc on 25/04/2016.
 */
public class Browse_song extends Activity {
    private FeatureCoverFlow mCoverFlow;
    private CoverFlowAdapter mAdapter;
    private ArrayList<GameEntity> mData = new ArrayList<>(0);
    private TextSwitcher mTitle;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();

    private long fileSize = 0;
    private static final int PREFERENCE_PRIVATE_MODE = 0;
    SharedPreferences sh_pref;
    SharedPreferences.Editor toEdit;
    LinearLayout l1;
    public static final int progress_bar_type = 0;
    private ListView lv;
    //   ArrayAdapter<String> adapter;
    EditText inputSearch;
    ArrayList<HashMap<String, String>> productList;
    private ProgressDialog prgDialog;
    String file_url1 = "https://soundcloud.com/storynory/the-valentine-witch-mp3";
    private static String file_url = "http://programmerguru.com/android-tutorial/wp-content/uploads/2013/04/hosannatelugu.mp3";
    HorizontalListView list1, list2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bwowse_song);
        sh_pref = getSharedPreferences("shref", PREFERENCE_PRIVATE_MODE);
        toEdit = sh_pref.edit();
        toEdit.putString("demo", "demo");
        toEdit.putString("file_url", file_url);
        toEdit.commit();
        list1 = (HorizontalListView) findViewById(R.id.hlvCustomList);
        list2 = (HorizontalListView) findViewById(R.id.hlvCustomList1);
        final String[] web = {
                "Google Plus",
                "Twitter",
                "Windows",
                "Bing",
                "Itunes",
                "Wordpress",
                "Drupal"
        };
        Integer[] imageId = {
                R.drawable.title_shrink,
                R.drawable.title_shrink,
                R.drawable.title_shrink,
                R.drawable.title_shrink,
                R.drawable.title_shrink,
                R.drawable.title_shrink,
                R.drawable.title_shrink

        };
        mData.add(new GameEntity(R.drawable.title_shrink, R.string.title1));
        mData.add(new GameEntity(R.drawable.profile, R.string.title2));
        mData.add(new GameEntity(R.drawable.title_shrink, R.string.title3));
        mData.add(new GameEntity(R.drawable.profile, R.string.title4));

        mTitle = (TextSwitcher) findViewById(R.id.title);
        mTitle.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                LayoutInflater inflater = LayoutInflater.from(Browse_song.this);
                TextView textView = (TextView) inflater.inflate(R.layout.item_title, null);
                return textView;
            }
        });
        Animation in = AnimationUtils.loadAnimation(this, R.anim.slide_in_top);
        Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        mTitle.setInAnimation(in);
        mTitle.setOutAnimation(out);

        mAdapter = new CoverFlowAdapter(this);
        mAdapter.setData(mData);
        mCoverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);
        mCoverFlow.setAdapter(mAdapter);

        mCoverFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               /* Toast.makeText(Browse_song.this,
                        getResources().getString(mData.get(position).titleResId),
                        Toast.LENGTH_SHORT).show();*/
                Intent i = new Intent(Browse_song.this, Title_screen.class);
                startActivity(i);

            }
        });

        mCoverFlow.setOnScrollPositionListener(new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                mTitle.setText(getResources().getString(mData.get(position).titleResId));

            }

            @Override
            public void onScrolling() {
                mTitle.setText("");
            }
        });
        String products[] = {"Dell Inspiron", "HTC One X", "HTC Wildfire S", "HTC Sense", "HTC Sensation XE",
                "iPhone 4S", "Samsung Galaxy Note 800",
                "Samsung Galaxy S3", "MacBook Air", "Mac Mini", "MacBook Pro"};
        l1 = (LinearLayout) findViewById(R.id.l1);
       /* l1 = (LinearLayout) findViewById(R.id.l1);
        lv = (ListView) findViewById(R.id.list_view);
        inputSearch = (EditText) findViewById(R.id.inputSearch);

        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.product_name, products);
        lv.setAdapter(adapter);
*/
        /**
         * Enabling Search Filter
         * */
        Custom_adapter adapter = new
                Custom_adapter(Browse_song.this, web, imageId);
        list1.setAdapter(adapter);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // Toast.makeText(Browse_song.this, "You Clicked at " + web[+position], Toast.LENGTH_SHORT).show();

            }
        });
        list2.setAdapter(adapter);
        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //  Toast.makeText(Browse_song.this, "You Clicked at " + web[+position], Toast.LENGTH_SHORT).show();

            }
        });
        /*inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                Browse_song.this.adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });*/
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Browse_song.this, Title_screen.class);
                startActivity(i);
            }
        });
    }
}
