package com.myapp.abc.music_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by abc on 25/04/2016.
 */
public class Forget_password extends Activity {
    LinearLayout returnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot);
        returnlogin = (LinearLayout) findViewById(R.id.returnlogin);
        returnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Forget_password.this, Login.class);
                startActivity(i);
            }
        });
    }
}
