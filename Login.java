package com.myapp.abc.music_app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

/**
 * Created by abc on 20/04/2016.
 */
public class Login extends Activity {
    TextView txt_forgpass, txt_or, txt_fb, btn_signup, btn_login;
    EditText ed_email, ed_pass;
    RelativeLayout fb_login;
    LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.login_screen);
        txt_forgpass = (TextView) findViewById(R.id.txt_forgpass);
        loginButton = (LoginButton) findViewById(R.id.fbb);
        txt_or = (TextView) findViewById(R.id.txt_or);
        txt_fb = (TextView) findViewById(R.id.textView_fb);
        btn_login = (TextView) findViewById(R.id.btn_login);
        btn_signup = (TextView) findViewById(R.id.btn_signup);
        ed_email = (EditText) findViewById(R.id.ed_email);
        ed_pass = (EditText) findViewById(R.id.ed_pass);
        String fontPath = "fonts/Hero_Light.otf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        txt_forgpass.setTypeface(tf);
        fb_login = (RelativeLayout) findViewById(R.id.fb_login);
        txt_or.setTypeface(tf);
        txt_fb.setTypeface(tf);
        ed_email.setTypeface(tf);
        ed_pass.setTypeface(tf);
        btn_signup.setTypeface(tf);
        btn_login.setTypeface(tf);
        TypefaceSpan typefaceSpan = new Custom_typeface(tf);
        SpannableString spannableString1 = new SpannableString("Email Address");
        spannableString1.setSpan(typefaceSpan, 0, spannableString1.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ed_email.setHint(spannableString1);
        SpannableString spannableString2 = new SpannableString("Password");
        spannableString1.setSpan(typefaceSpan, 0, spannableString2.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ed_pass.setHint(spannableString2);
        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday")); // ####### Facebook Sign In Coding
        // loginButton.registerCallback(callbackManager, mCallback);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Registration.class);
                startActivity(i);
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Drawer.class);
                startActivity(i);
            }
        });
        fb_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        txt_forgpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Login.this, Forget_password.class);
                startActivity(i);
            }
        });
        loginButton.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Intent i = new Intent(Login.this, Drawer.class);
                startActivity(i);
               /* ed_email.setText(
                        "User ID: "
                                + loginResult.getAccessToken().getUserId()
                                + "\n" +
                                "Auth Token: "
                                + loginResult.getAccessToken().getToken()
                );
*/

            }


            @Override
            public void onCancel() {
                // ed_email.setText("Login attempt canceled.");

            }

            @Override
            public void onError(FacebookException e) {
                //  ed_email.setText("Login attempt failed.");

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            ed_pass.setText(profile.getName());
            Log.e("pname", profile.getFirstName() + "abccccccc");
        }
    }

}
