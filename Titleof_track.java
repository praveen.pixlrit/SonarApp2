package com.myapp.abc.music_app;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by abc on 22/04/2016.
 */
public class Titleof_track extends Activity implements MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener, View.OnClickListener {
    ImageView pause, btn_back, btn_forword, volume, repeat;
    MediaPlayer mp;
    SeekBar seekBar;
    TextView ftime, ltime;
    File path, from, to;
    private int seekForwardTime = 5000; // 5000 milliseconds
    private int seekBackwardTime = 5000;
    private Utilities utils;
    private Handler mHandler = new Handler();
    private boolean isShuffle = false;
    private boolean isRepeat = false;
    private int currentSongIndex = 0;
    Boolean i;
    private static final int PREFERENCE_PRIVATE_MODE = 0;
    SharedPreferences sh_pref;
    SharedPreferences.Editor toEdit;
    String song_date;
    String current_date;
    long difference_date;
    String dayDifference;
    int myno = 0;
    ImageButton imageButton_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        sh_pref = getSharedPreferences("shref", PREFERENCE_PRIVATE_MODE);
        toEdit = sh_pref.edit();
        path = android.os.Environment.getExternalStorageDirectory();
        if (sh_pref.contains("demo")) {
            song_date = sh_pref.getString("date", "");
            Log.e("song_date", song_date);
        }

        imageButton_back = (ImageButton) findViewById(R.id.imgLeftMenu);

        mp = new MediaPlayer();
        setContentView(R.layout.title_of_track);
        pause = (ImageView) findViewById(R.id.pose);
        repeat = (ImageView) findViewById(R.id.repeat);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        ftime = (TextView) findViewById(R.id.firsttime);
        ltime = (TextView) findViewById(R.id.lasttime);
        btn_back = (ImageView) findViewById(R.id.backword);
        btn_forword = (ImageView) findViewById(R.id.forword);
        volume = (ImageView) findViewById(R.id.volume);
        utils = new Utilities();
        seekBar.setOnSeekBarChangeListener(this); // Important
        mp.setOnCompletionListener(this);

        playSong();
     /*   Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        Log.e("newtime", formattedDate);
        String[] separated = formattedDate.split(" ");
        current_date = separated[0];
        String time = separated[1];
        Log.e("current_date", current_date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = simpleDateFormat.parse("08:00 AM");
            endDate = simpleDateFormat.parse("04:00 PM");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.e("stendate", startDate + "  " + endDate);
        playSong();
        long difference = endDate.getTime() - startDate.getTime();
       *//* if (difference <= 5) {
            playSong();
        } else {
            Toast.makeText(getApplicationContext(), "Timeout", Toast.LENGTH_LONG).show();
        }*//*
        Date date1, date2;
        SimpleDateFormat dates = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date1 = dates.parse(current_date);
            date2 = dates.parse(song_date);
            difference_date = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);
            dayDifference = Long.toString(differenceDates);
            Log.e("d1d2", dayDifference);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            myno = Integer.parseInt(dayDifference);
            Log.e("myno", String.valueOf(myno));
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        if (myno <= 1) {
            playSong();

        } else {
            Toast.makeText(getApplicationContext(), "Timeout", Toast.LENGTH_LONG).show();
        }*/
        volume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i = true) {
                    i = false;
                    mute();
                    mp.pause();
                    // Changing button image to play button
                    pause.setImageResource(R.drawable.ic_action_playbutton);
                }
            }
        });
        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.setLooping(true);
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // check for already playing
                unmute();
                if (mp.isPlaying()) {
                    if (mp != null) {
                        mp.pause();
                        // Changing button image to play button
                        pause.setImageResource(R.drawable.ic_action_playbutton);
                    }
                } else {
                    // Resume song
                    if (mp != null) {
                        mp.start();
                        // Changing button image to pause button
                        pause.setImageResource(R.drawable.ic_action_play3);
                    }
                }

            }
        });
        btn_forword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // get current song position
                int currentPosition = mp.getCurrentPosition();
                // check if seekForward time is lesser than song duration
                if (currentPosition + seekForwardTime <= mp.getDuration()) {
                    // forward song
                    mp.seekTo(currentPosition + seekForwardTime);
                } else {
                    // forward to end position
                    mp.seekTo(mp.getDuration());
                }
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // get current song position
                int currentPosition = mp.getCurrentPosition();
                // check if seekBackward time is greater than 0 sec
                if (currentPosition - seekBackwardTime >= 0) {
                    // forward song
                    mp.seekTo(currentPosition - seekBackwardTime);
                } else {
                    // backward to starting position
                    mp.seekTo(0);
                }

            }
        });

    }

    public void playSong() {
        // Play song

        if (path.exists()) {
            from = new File(path, "/.zzzzzzzzzzzz123.txt");
            to = new File(path, "/.zzzzzzzzzzzz123.mp3");
            if (from.exists())
                from.renameTo(to);
        }
        try {
            Log.e("path", path.toString());
            mp.setDataSource(path + "/.zzzzzzzzzzzz123.mp3");//Write your location here
            mp.prepare();
            mp.start();
            pause.setImageResource(R.drawable.ic_action_play3);
            seekBar.setProgress(0);
            seekBar.setMax(100);
            updateProgressBar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    /*    try {
            mp.setDataSource(path + "/.newsong.mp3");
            mp.prepare();
            mp.start();
            // Displaying Song title
            //     String songTitle = songsList.get(songIndex).get("songTitle");
            //songTitleLabel.setText(songTitle);

            // Changing Button Image to pause image
            pause.setImageResource(R.drawable.ic_action_play3);

            // set Progress bar values
            seekBar.setProgress(0);
            seekBar.setMax(100);

            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {

        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            if (mp != null) {
                long totalDuration = mp.getDuration();
                long currentDuration = mp.getCurrentPosition();

                // Displaying Total Duration time
                ltime.setText("" + utils.milliSecondsToTimer(totalDuration));
                // Displaying time completed playing
                ftime.setText("" + utils.milliSecondsToTimer(currentDuration));

                // Updating progress bar
                int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));
                //Log.d("Progress", ""+progress);
                seekBar.setProgress(progress);

                // Running this thread after 100 milliseconds
                mHandler.postDelayed(this, 100);
            }
        }
    };

    /**
     *
     * */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
        // seekBarValue.setText(String.valueOf(progress));
        Log.e("seekvalue", String.valueOf(progress));
    }

    /**
     * When user starts moving the progress handler
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        // mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mp.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        mp.seekTo(currentPosition);

        // update timer progress again
        updateProgressBar();
    }

    /**
     * On Song Playing completed
     * if repeat is ON play same song again
     * if shuffle is ON play random song
     */
   /* @Override
    public void onCompletion(MediaPlayer arg0) {

        // check for repeat is ON or OFF
        if (isRepeat) {
            // repeat is on play same song again
            playSong(currentSongIndex);
        } else if (isShuffle) {
            // shuffle is on - play a random song
            Random rand = new Random();
            currentSongIndex = rand.nextInt((songsList.size() - 1) - 0 + 1) + 0;
            playSong(currentSongIndex);
        } else {
            // no repeat or shuffle ON - play next song
            if (currentSongIndex < (songsList.size() - 1)) {
                playSong(currentSongIndex + 1);
                currentSongIndex = currentSongIndex + 1;
            } else {
                // play first song
                playSong(0);
                currentSongIndex = 0;
            }
        }
    }
*/
    @Override
    public void onDestroy() {
        super.onDestroy();
        mp.release();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {

    }

    private void mute() {
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        am.setStreamMute(AudioManager.STREAM_MUSIC, true);
    }

    public void unmute() {
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        am.setStreamMute(AudioManager.STREAM_MUSIC, false);
    }


    @Override
    public void onBackPressed() {
        if (mp != null) {
            mp.release();

            if (mHandler != null)
                mHandler.removeCallbacks(mUpdateTimeTask);
        }
        if (path.exists()) {
            from = new File(path, "/.zzzzzzzzzzzz123.mp3");
            to = new File(path, "/.zzzzzzzzzzzz123.txt");
            if (from.exists())
                from.renameTo(to);
        }
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (path.exists()) {
            from = new File(path, "/.zzzzzzzzzzzz123.mp3");
            to = new File(path, "/.zzzzzzzzzzzz123.txt");
            if (from.exists())
                from.renameTo(to);
        }
        if (mHandler != null)
            mHandler.removeCallbacks(null);
    }

    @Override
    public void onClick(View v) {
        if (path.exists()) {
            from = new File(path, "/.zzzzzzzzzzzz123.mp3");
            to = new File(path, "/.zzzzzzzzzzzz123.txt");
            if (from.exists())
                from.renameTo(to);
        }
        if (mp != null) {
            mp.release();

            if (mHandler != null)
                mHandler.removeCallbacks(mUpdateTimeTask);
        }
    }
}
